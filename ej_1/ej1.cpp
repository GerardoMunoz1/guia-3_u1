#include <iostream>

using namespace std;

struct Nodo{
    int dato;
    Nodo *siguiente;
};

void insertarLista(Nodo *&, int);

void mostrarLista(Nodo *);

int main(){
    Nodo *lista = NULL;
    int dato, salir;

    do{
        cout << "Digite un numero: ";
        cin >> dato;
        insertarLista(lista, dato);
        mostrarLista(lista);
        cout << "\n¿Salir? (Si=1;No=0): ";
        cin >> salir;


    }while(salir != 1);

    return 0;
}

void insertarLista(Nodo *&lista, int n){
    Nodo *nuevo_nodo = new Nodo();
    nuevo_nodo->dato = n;

    Nodo *aux1 = lista;
    Nodo *aux2;

    while((aux1 != NULL) && (aux1->dato <n)){
        aux2 = aux1;
        aux1 = aux1->siguiente;
    }

    if (lista == aux1){
        lista = nuevo_nodo;
    }
    else{
        aux2->siguiente = nuevo_nodo;
    }
    nuevo_nodo->siguiente = aux1;

    cout << "\tElemento " << n << " insertado a la lista correctamente.\n";
}

void mostrarLista(Nodo *lista){
    Nodo *actual = new Nodo();
    actual = lista;

    while (actual != NULL){
        cout << actual->dato << " -> ";
        actual = actual->siguiente;
    }
}
